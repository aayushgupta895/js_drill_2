// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function problem4(inventory) {
  if (!Array.isArray(inventory)) {
    return `input is not an array`;
  }
  const carYearList = inventory.reduce((list, carObj) => {
    list.push(carObj["car_year"]);
    return list;
  }, []);
  return carYearList.length == 0 ? `car's years not present` : carYearList;
}

module.exports = problem4;
