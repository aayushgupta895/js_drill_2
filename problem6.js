// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function problem6(inventory) {
  if (!Array.isArray(inventory)) {
    return `input is not an array`;
  }
  const BMWAndAudi = inventory.reduce((list, car)=>{
    if (car["car_make"] == "BMW" || car["car_make"] == "Audi") {
        list.push(car);
    }
    return list;
  }, [])
  return BMWAndAudi.length == 0
    ? `there are no cars available made by BMW and Audi`
    : JSON.stringify(BMWAndAudi);
}

module.exports = problem6;
