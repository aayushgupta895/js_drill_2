// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
"Car 33 is a *car year goes here* *car make goes here* *car model goes here*";

function problem1(inventory, carId) {
  if (!Array.isArray(inventory)) {
    return `input is not an array`;
  }
  const carObj = inventory.filter((carObject) => {
    if (Object.keys(carObject).includes("id")) {
      return carObject["id"] == carId;
    } else {
      return false;
    }
  });
  if (carObj.length == 0) {
    return `the car is not present`;
  }
  if (carObj[0] != null && typeof carObj[0] == "object") {
    const carObjKeys = Object.keys(carObj[0]);
    const ans =
      (carObjKeys.includes("car_make")
        ? ` car is a '${carObj[0]["car_make"]}'`
        : ``) +
      (carObjKeys.includes("car_model")
        ? ` the  car model is '${carObj[0]["car_model"]}'`
        : ``) +
      (carObjKeys.includes("car_year")
        ? ` car is made on the year '${carObj[0]["car_year"]}'`
        : ``);
    return ans == `` ? `only car's id is present, no details found` : ans;
  }
  return `The car is not present`;
}

module.exports = problem1;
