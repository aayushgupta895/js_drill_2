// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

const getCarYears = require("./problem4");

function problem5(inventory) {
  if (!Array.isArray(inventory)) {
    return `input is not an array`;
  }
  const carYearList = getCarYears(inventory);
  if (typeof carYearList == "string" || carYearList.length == 0) {
    return 0;
  }
  const olderCars = carYearList.reduce((year, carYear) =>{
    if (carYear < 2000) {
      year.push(carYear);
    }
    return year;
  }, []);
  return olderCars == 0 ? `there are no older cars` : olderCars;
}

module.exports = problem5;
