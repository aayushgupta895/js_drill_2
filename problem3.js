// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function problem3(inventory) {
  if (!Array.isArray(inventory)) {
    return `input is not an array`;
  }
  const modelList = inventory.reduce((list, carObj) =>{
    if(Object.keys(carObj).includes('car_model')){
      list.push(carObj['car_model']);
    }
    return list;
  }, []);
  modelList.sort((list1, list2) => list1.localeCompare(list2));

  return modelList;
}

module.exports = problem3;
